#include "SpaceWorld.h"
#include "AsteroidObject.h"
#include "ImageLibrary.h"
#include "CurrentGameWorld.h"
#include "BackgroundObject.h"
int asteroidCount = 0;

SpaceWorld::SpaceWorld(void)
{
}


SpaceWorld::~SpaceWorld(void)
{
}

void SpaceWorld::OnCreate()
{
	int size = 20;
	int width = 10;
	for(int i = 0 ; i < size ; i ++)
	{
		for(int j = 0 ; j < size ; j ++)
		{
			BackgroundObject * object = new BackgroundObject();
			object->SetPosition(Vector2((int)(i - 10)*width, (int)(j - 10)*width));
			object->SetTexture(ImageLibrary::STARS_IMG);
			CurrentGameWorld::CURRENTWORLD->Add(object);
		}
	}
}

void SpaceWorld::OnUpdate()
{
	if(asteroidCount < 100)
	{
		
		AsteroidObject * asteroid = new AsteroidObject();
		asteroid->SetTexture(ImageLibrary::ASTEROID_IMG);
		float x, y;
		x = (rand() % 2000 - 1000) / 10.0f;
		y = (rand() % 2000 - 1000) / 10.0f;
		asteroid->SetPosition(Vector2(x,y));
		Add(asteroid);
		asteroidCount++;
		
	}
}
