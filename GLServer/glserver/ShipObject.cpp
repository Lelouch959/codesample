#include "ShipObject.h"
#include "AsteroidObject.h"
#include "ObjectTypes.h"
#include "ProjectileObject.h"
#include "ImageLibrary.h"
#include "CurrentGameWorld.h"
#include <math.h>
ShipObject::ShipObject(void)  : GameObject()
{
	width = 1.5f;
	height = 1.5f;
	position = velocity = Vector2(0,0);
	type = GAMEOBJECT_PLAYER;
	//start facing up
	direction = Vector2(0,1.0f);
	rotation = 0;
	fireDelay = 0;
	damageDelay = 0;
	lastFired = NULL;
	health = 5;
	Create();
}


ShipObject::~ShipObject(void)
{
}

void ShipObject::TurnLeft()
{
	//rotate all to the left
	Rotate(3);
}

void ShipObject::TurnRight()
{
	//rotate all the the right
	Rotate(-3);
}

void ShipObject::Accelerate()
{
	
	velocity = velocity + direction;
	float magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y));
	if(magnitude > 15)
		velocity = velocity - direction;

	
}

void ShipObject::Decelerate()
{
	velocity = velocity - (velocity*0.1f);
	if(velocity.x < 1 || velocity.x > -1)
		velocity.x = 0;
	if(velocity.y < 1 || velocity.y > -1)
		velocity.y = 0;
}

void ShipObject::Fire()
{
	//create projectile
	if(fireDelay > 1.0f)
	{
		ProjectileObject * lazer = new ProjectileObject();
		lazer->SetPosition(Vector2(position.x + (direction.x * width/2.0f),position.y + (direction.y * width/2.0f)));
		lazer->SetVelocity(direction * 8.0f);
		lazer->SetTexture(ImageLibrary::LAZER_IMG);
		lazer->Rotate(rotation+90);
		lastFired = lazer;
		CurrentGameWorld::CURRENTWORLD->Add(lazer);
		fireDelay = 0;
	}

}

int ShipObject::GetHealth()
{
	return health;
}

void ShipObject::OnUpdate(float deltaTime)
{
	fireDelay += deltaTime;
	damageDelay += deltaTime;
}

bool ShipObject::CollidesWithType(int tType)
{
	if(tType == GAMEOBJECT_ASTEROID || tType == GAMEOBJECT_PROJECTILE)
		return true;
	else
		return false;
}

void ShipObject::OnCollision(GameObject * otherObject)
{
	//action for the asteroid, when it hits the ship
	//asteroid flagged to be removed here, ship will be damage in the ship code. 
	if(otherObject->GetType() == GAMEOBJECT_ASTEROID )
	{
		velocity = velocity * -1.0f;
		if(damageDelay > 2.0f)
		{
			health -=1;
			damageDelay = 0;
		}
	}
	if(otherObject->GetType() == GAMEOBJECT_PROJECTILE)
	{
		if(otherObject != lastFired)
		{
			if(damageDelay > 2.0f)
			{
				health -=1;
				damageDelay = 0;
			}
		}
	}
}
