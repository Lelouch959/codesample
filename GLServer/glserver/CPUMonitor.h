#pragma once
#include "windows.h"
class CPUMonitor
{
public:
	CPUMonitor(void);
	void init();
	double getCurrentValue();
	ULARGE_INTEGER lastCPU, lastSysCPU, lastUserCPU;
	int numProcessors;
	HANDLE self;
};

