#pragma once

#include <glut.h>
class ImageLibrary
{
public:
	static void LoadImages();

	static GLuint SHIP_IMG;
	static GLuint ASTEROID_IMG;
	static GLuint LAZER_IMG;
	static GLuint STARS_IMG;
	static GLuint MEMORYUSER[100];
};

