#include "ImageLibrary.h"
#include "SOIL\SOIL.h"
#include <stdio.h>
#include <stdlib.h>

#include "resource.h"
#include "resource1.h"

GLuint ImageLibrary::SHIP_IMG = 0;
GLuint ImageLibrary::ASTEROID_IMG = 0;
GLuint ImageLibrary::LAZER_IMG = 0;
GLuint ImageLibrary::STARS_IMG = 0;
GLuint ImageLibrary::MEMORYUSER[100];
void ImageLibrary::LoadImages()
{
	ImageLibrary library;
	//Load images
	library.SHIP_IMG = SOIL_load_OGL_texture("../ship.png",SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID, SOIL_FLAG_MULTIPLY_ALPHA );
	if( 0 == ImageLibrary::SHIP_IMG)
	{
		printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
	}
	library.ASTEROID_IMG =  SOIL_load_OGL_texture("../asteroid.png",SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID, SOIL_FLAG_MULTIPLY_ALPHA );
	if( 0 == ImageLibrary::ASTEROID_IMG )
	{
		printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
	}
	library.LAZER_IMG =  SOIL_load_OGL_texture("../redLaserRay.png",SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID, SOIL_FLAG_MULTIPLY_ALPHA );
	if( 0 == ImageLibrary::LAZER_IMG )
	{
		printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
	}
	library.STARS_IMG =  SOIL_load_OGL_texture("../stars.jpg",SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID, SOIL_FLAG_MULTIPLY_ALPHA );
	if( 0 == ImageLibrary::STARS_IMG )
	{
		printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
	}

	//Images to show memory does not expand with users for images.
	//for(int i = 0 ; i < 100 ; i ++)
	//{
	//	library.MEMORYUSER[i] = SOIL_load_OGL_texture("../stars.jpg",SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID, SOIL_FLAG_MULTIPLY_ALPHA );
	//}
}
