#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>

#include "User.h"

#pragma comment(lib, "Ws2_32.lib")
class Server
{

public:

	#define DEFAULT_PORT        5150
	#define DEFAULT_BUFFER      4096

	Server(void);
	~Server(void);

	bool initialise();
	User* acceptNewConnections(int*);
	void setData(char * pixelData);

	
    SOCKET        sListen;
	int    iPort;
	BOOL   bInterface;
	BOOL   bRecvOnly;
	char   szAddress[128];       // Interface to listen for clients on
    char *  dataBuffer;
};

