// MMOServer.cpp : Defines the entry point for the console application.
//


#include <stdio.h>
#include <stdlib.h>
#include <glew.h>
#include <glut.h>


#pragma comment(lib, "glew32.lib")

#include <time.h>
#include <assert.h>
#include "Server.h"
#include <Windows.h>
#include "resource.h"
#include "resource1.h"
#include "ShipObject.h"
#include "SpaceWorld.h"
#include "CurrentGameWorld.h"
#include "SOIL\SOIL.h"
#include "AsteroidObject.h"
#include "ImageLibrary.h"
#include "psapi.h"
#include "CPUMonitor.h"

// PBO macro (see spec for details)
#define BUFFER_OFFSET(i) ((char *)NULL + (i))


// array size N is texsize*texsize
static const int texSize = 500;
static const int N = texSize*texSize;

//
// Enable this #define to enable PBOs for glTexSubImage() and glReadPixels().
// Otherwise, PBOs are not used.
//
#define USE_PBO
#define GLEW_STATIC

//main methods
void init(int argc, char **argv);
void display();
DWORD WINAPI acceptConnections(LPVOID lpParam);
DWORD WINAPI UpdateGame(LPVOID lpParam);
void handleResize(int w, int h);


GLuint ioBuf[2];
int picWidth;
int picHeight;
Server * server;
int * connectionCount = 0;
char * pixelData;
std::vector<User*> usersConnected;

float updateTiming = 0;
int updateCount = 0;
float renderTiming = 0;
int renderCount = 0;
int lastRenderTime = 0;
int currentRenderTime = 0;

CPUMonitor cpuMonitor;

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int main(int argc, char **argv) 
{

	cpuMonitor = CPUMonitor();
	cpuMonitor.init();

	 MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);
	DWORDLONG totalVirtualMem = memInfo.ullTotalPageFile;
	DWORDLONG totalPhysMem = memInfo.ullTotalPhys;
	 printf("TotalVirtualMemory      :: %I64u \n", totalVirtualMem  / 1024 / 1024);
	 printf("PhysicalTotalMemory   	:: %I64u \n", totalPhysMem / 1024 / 1024);

	CurrentGameWorld::CURRENTWORLD = new SpaceWorld();
	usersConnected = std::vector<User*>();
	//begin the openGL first, as may be used by objects. 
    init(argc, argv);
	
	ImageLibrary::LoadImages();

	//thread to accept new users
	 HANDLE        hServerThread;
     DWORD         dwThreadId;
	 hServerThread = CreateThread(NULL, 0, acceptConnections,NULL,  0, &dwThreadId);
	 

	//thread to update game
	  HANDLE        hUpdateThread;
     DWORD         dwDupateThreadId;
	 hUpdateThread = CreateThread(NULL, 0, UpdateGame,NULL,  0, &dwDupateThreadId);
	 SetThreadPriority(hUpdateThread, THREAD_MODE_BACKGROUND_BEGIN);
	 SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL);

	
	//begin draw
	glutMainLoop();
    
	return 0;
}

DWORD WINAPI acceptConnections(LPVOID lpParam)
{
	while(true)
	{
		User * newUser = server->acceptNewConnections(connectionCount);
		if(newUser != NULL)
		{
			ShipObject * playerShip = new ShipObject();
			playerShip->SetTexture(ImageLibrary::SHIP_IMG);
			CurrentGameWorld::CURRENTWORLD->Add(playerShip);
			newUser->AddPlayerUnit(playerShip);
			usersConnected.push_back(newUser);
		}
	}
	return 0;
}

DWORD WINAPI  UpdateGame(LPVOID lpParam)
{
	int lastTime = GetTickCount();
	CurrentGameWorld::CURRENTWORLD->OnCreate();
	while(true)
	{
		
		MEMORYSTATUSEX memInfo;
		memInfo.dwLength = sizeof(MEMORYSTATUSEX);
		GlobalMemoryStatusEx(&memInfo);
		
		PROCESS_MEMORY_COUNTERS_EX pmc;
		GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
		SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;
		SIZE_T physMemUsedByMe = pmc.WorkingSetSize;
		int currentTime = GetTickCount();
		float deltaTime = (currentTime - lastTime) * 0.001f;
		
		if(deltaTime < 0.02) //to prevent extra update loops, it is capped at around 50 - 60 updates per second, to free up space for rendering. if the delay exceeds the sleep time, it will be ignored. 
			Sleep(20);

		updateTiming+= deltaTime; // delta time is seconds, 
		if(updateTiming > 1.0f)//a second past
		{
			updateTiming = 0; //reset clock for a second
			printf("Update :: Clients Connected : %d, UpdatesPerSecond  %d \n",usersConnected.size(), updateCount);	
			printf("UsedVirtualMemory       :: %I64u \n",( memInfo.ullTotalPageFile - memInfo.ullAvailPageFile )  / 1024 / 1024);
			printf("AppVirtualUsedMemory    :: %I64u \n", virtualMemUsedByMe  / 1024 / 1024);
			printf("PhysicalUsedMemory   	:: %I64u \n", physMemUsedByMe  / 1024 / 1024);
			printf("CPU Usage               :: %f \n", cpuMonitor.getCurrentValue());
			updateCount = 0;
		}
		lastTime = currentTime;

		CurrentGameWorld::CURRENTWORLD->AddAllToSpawn();
		CurrentGameWorld::CURRENTWORLD->Update(deltaTime);
		for(int i = 0 ; i < usersConnected.size() ; i++)
		{
			usersConnected[i]->ControlUpdate();
		}
		CurrentGameWorld::CURRENTWORLD->RemoveDead();
		updateCount++;
		lastTime = currentTime;
		
	}
	return 0;
}


void init(int argc, char **argv)
{
	
	//Begin the server

	server = new Server();
	server->initialise();

	//
    // set up glut to get valid GL context and 
    // get extension entry points
    //
    glutInit (&argc, argv);

	unsigned int displayMode = GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH;

	int width = 500;
	int height = 500;

	glutInitDisplayMode (displayMode);
	
	glutInitWindowPosition (50, 50);
	glutInitWindowSize (width, height); 
    glutCreateWindow("STREAMING TUTORIAL");
	glutDisplayFunc(display);
    glewInit();
	
	handleResize(width, height);

    glGenBuffersARB(2, ioBuf);
	glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, ioBuf[0]);	
	glBufferDataARB(GL_PIXEL_PACK_BUFFER_ARB, texSize*texSize * 3 * sizeof(GLbyte), NULL, GL_STREAM_READ_ARB);
	glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, ioBuf[1]);	
	glBufferDataARB(GL_PIXEL_PACK_BUFFER_ARB, texSize*texSize * 3 * sizeof(GLbyte), NULL, GL_STREAM_READ_ARB);

	glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);
	
}

void display()
{
	currentRenderTime = GetTickCount();
	float deltaTime = (	currentRenderTime - lastRenderTime) * 0.001f;
	renderTiming+= deltaTime; // delta time is seconds, 
	if(renderTiming > 1.0f)//a second past
	{
		renderTiming = 0; //reset clock for a second
		printf("Render :: Clients Connected : %d, RenderLoopsPerSecond  %d \n",usersConnected.size(), renderCount);
		renderCount = 0;
	}
	lastRenderTime = currentRenderTime;
	for(int i = 0 ; i < usersConnected.size() ; i ++)
	{
		User * currentDrawUser = usersConnected[i];
		glClearColor(0.0f,0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		
		glMatrixMode(GL_PROJECTION); //Switch to setting the camera perspective
		//Set the camera perspective
		glLoadIdentity(); //Reset the camera
		gluPerspective(45.0,                  //The camera angle
					   (double)500 / (double)500, //The width-to-height ratio
					   0.0,                   //The near z clipping coordinate
					   200.0);                //The far z clipping coordinate

		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glMatrixMode(GL_MODELVIEW); //Switch to the drawing perspective

		glLoadIdentity(); //Reset the drawing perspective 
		glColor3f(1.0f,1.0f,1.0f);
		gluLookAt(currentDrawUser->GetX(), currentDrawUser->GetY(),20,currentDrawUser->GetX(), currentDrawUser->GetY(), 0, 0,-1,0 );
		
		CurrentGameWorld::CURRENTWORLD->Draw(Vector2(currentDrawUser->GetX(),currentDrawUser->GetY()));

		glFinish();
		//Set the camera perspective
		glMatrixMode(GL_PROJECTION); //Switch to setting the camera perspective
		glLoadIdentity(); //Reset the camera
		gluOrtho2D(0,500,500,0); //setup for interfaceDraw

		glMatrixMode(GL_MODELVIEW); //Switch to the drawing perspective

		glLoadIdentity(); //Reset the drawing perspective 

		currentDrawUser->Draw();

		glDisable(GL_BLEND);

		glFinish();
		glReadBuffer(GL_BACK);

		glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, ioBuf[0]);
		glReadPixels(0, 0, texSize, texSize, GL_RGB, GL_UNSIGNED_BYTE, BUFFER_OFFSET(0));

		//glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, ioBuf[1]);
		char* pixels = (char*)glMapBufferARB(GL_PIXEL_PACK_BUFFER_ARB, GL_READ_ONLY_ARB);
		assert(pixels); // we are in trouble
		
		pixelData = new char[texSize*texSize*3];
		//double* results = new double[N];
		for (int j = 0; j < N *3; j++)
		{
			pixelData[j] = pixels[j];
		}
		usersConnected[i]->SetData(pixelData);
		glUnmapBuffer(GL_PIXEL_PACK_BUFFER_ARB);
		glBindBufferARB(GL_PIXEL_PACK_BUFFER_ARB, 0);
	}
	renderCount++;
	//glutSwapBuffers(); //Send the 3D scene to the screen
	glutPostRedisplay();
}

void handleResize(int x, int y) {
	//Tell OpenGL how to convert from coordinates to pixel values
	glViewport(x, y, 500, 500);
	
}

