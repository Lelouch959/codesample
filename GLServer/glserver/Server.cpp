
#include "Server.h"


Server::Server(void)
{
	iPort      = DEFAULT_PORT; 
	bInterface = FALSE,	 
    bRecvOnly  = FALSE;  
}


Server::~Server(void)
{

}

DWORD WINAPI ClientThread(LPVOID lpParam)
{
	User *   user=(User*)lpParam;
	char          controls[5] = {0,0,0,0,0};
    int           ret,
                  nLeft,
                  idx;
	bool complete = false;
	while(!complete)
    {
		//recieve controlls, when ready for pixels return
		ret = recv(user->sock, controls, 5, 0);
		if (ret == 0)       
			break;
		else if (ret == SOCKET_ERROR)
		{
			break;
		}
		user->SetControls(controls);

		// send pixel data
		char * buffer = NULL;
		while(buffer == NULL)
		{
			if(user->buffer[user->completeBufferIndex] != NULL)
			{
				buffer = user->buffer[user->completeBufferIndex];
				user->buffer[user->completeBufferIndex] = NULL;
			}
		}
		ret = send(user->sock,buffer, 500*500*3, 0);
		if (ret == 0)
			break;
		else if (ret == SOCKET_ERROR)
		{
			break;
		}
		delete buffer;
		OutputDebugStringW(L"I finished a client thread");
		//complete = true;
	}
    
    return 0;
}

bool Server::initialise()
{
	WSADATA       wsd;
   
    struct sockaddr_in local;
     
	
	OutputDebugStringW(L"Server Started");
    if (WSAStartup(MAKEWORD(2,2), &wsd) != 0)
    {
        return 1;
    }

    // Create our listening socket
    //
    sListen = socket(AF_INET, SOCK_STREAM  , 0);
    if (sListen == SOCKET_ERROR)
    {
        return 1;
    }

    // Select the local interface and bind to it
    //
    if (bInterface)
    {
        local.sin_addr.s_addr = inet_addr(szAddress);
		return 1;
    }
    else
        local.sin_addr.s_addr = htonl(INADDR_ANY);
    local.sin_family = AF_INET;
    local.sin_port = htons(iPort);

    if (bind(sListen, (struct sockaddr *)&local, sizeof(local)) == SOCKET_ERROR)
    {
        return 1;
    }
    listen(sListen, 8);

   // closesocket(sListen);

   // WSACleanup();
    return 0;
}

User* Server::acceptNewConnections(int* connectionCount)
{

	int           iAddrSize;
    HANDLE        hThread;
	struct sockaddr_in client;
	SOCKET		  sClient;
    DWORD         dwThreadId;

    iAddrSize = sizeof(client);
    sClient = accept(sListen, (struct sockaddr *)&client,
                    &iAddrSize);
    if (sClient == INVALID_SOCKET)
    {
        return NULL;
    }
	User * user = new User();
	//initialise the collection of buffers to NULL
	user->sock = sClient;

    hThread = CreateThread(NULL, 0, ClientThread,(LPVOID)user,  0, &dwThreadId);
	connectionCount++;
    if (hThread == NULL)
    {
        return NULL;
    }
    CloseHandle(hThread);

	return user;
    
}

