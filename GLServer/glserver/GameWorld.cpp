#include "GameWorld.h"


GameWorld::GameWorld(void)
{
}


GameWorld::~GameWorld(void)
{
}

void GameWorld::Add(GameObject * object)
{
	mObjectsToAdd.push_back(object);
}

void GameWorld::AddAllToSpawn()
{
	for(int i = 0 ; i < mObjectsToAdd.size(); i ++)
	{
		mObjects.push_back(mObjectsToAdd[i]);
	}
	mObjectsToAdd.clear();
}



void GameWorld::Update(float deltaTime)
{
	
	int size = mObjects.size();
	for(int i = 0 ; i < size ; i ++ )
	{
		mObjects[i]->Update(deltaTime);
	}

	for(int i = 0 ; i < size ; i ++) //this will check everything against everything twice, so optimise
	{
		for(int j = 0 ; j < size ; j++)
		{
			if(i != j)
			{
				//check collision rules
				mObjects[i]->CollisionCheck(mObjects[j]);
			}
		}
	}

	OnUpdate(); //call virtual update method for custom update behaviour. 
}

void GameWorld::RemoveDead()
{
	for(int i = 0 ; i < mObjects.size() ; i ++ )
	{
		if(mObjects[i]->ShouldDie())
		{
			mObjects[i]->OnDestroy();
			delete mObjects[i];
			mObjects.erase(mObjects.begin() + i);
			i--;
		}
	}
}

void GameWorld::Draw(Vector2 centrePoint)
{
	//move camera to user position, 
	//only draw objects within area of user. 
	for(int i = 0 ; i < mObjects.size() ; i ++ )
	{
		if(mObjects[i]->GetPosition().x < centrePoint.x + 20 && mObjects[i]->GetPosition().x > centrePoint.x - 20
			&& mObjects[i]->GetPosition().y < centrePoint.y + 20 && mObjects[i]->GetPosition().y > centrePoint.y - 20)
			mObjects[i]->Draw();
	}
}
