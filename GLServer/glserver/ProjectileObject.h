#pragma once
#include "gameobject.h"
class ProjectileObject :
	public GameObject
{
public:
	ProjectileObject(void);
	~ProjectileObject(void);

	bool CollidesWithType(int);
	void OnCollision(GameObject *);
	void OnUpdate(float deltaTime);


	float timeSinceSpawn;
};

