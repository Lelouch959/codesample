#pragma once

#include <winsock2.h>
#include "ShipObject.h"

#define MAXBUFFERS 5
#define NUMCONTROLS 5
#define LEFT 0
#define UP 1
#define RIGHT 2
#define DOWN 3
#define FIRE 4
class User
{

public:
	User(void);
	~User(void);

	void ControlUpdate();
	void SetData(char*);
	void SetControls(char*);
	void AddPlayerUnit(ShipObject*);
	float GetX();
	float GetY();
	void Draw(); //mainly for interface or user specific draw

	ShipObject * playerUnit;
	BYTE* controls;
	SOCKET sock;
	char** buffer;
	int completeBufferIndex;

};

