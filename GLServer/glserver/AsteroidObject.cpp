#include "AsteroidObject.h"
#include "ShipObject.h"
#include <random>
#include "CurrentGameWorld.h"
#include "ImageLibrary.h"
AsteroidObject::AsteroidObject()
{
	position = Vector2(0,0);
	width = 2.0f;
	height = 2.0f;
	type = GAMEOBJECT_ASTEROID;
	float velX = (rand() % 10 + 1) - 5;
	float velY = (rand() % 10 + 1) - 5;
	velocity = Vector2(velX,velY);
	isDead = false;
	rotation = 0;
	Create();
}


AsteroidObject::~AsteroidObject(void)
{
}

void AsteroidObject::HalfSize()
{
	width = 1.0f;
	height  = 1.0f;
}
bool AsteroidObject::CollidesWithType(int tType)
{
	if(tType == GAMEOBJECT_ASTEROID || tType == GAMEOBJECT_PLAYER || tType == GAMEOBJECT_PROJECTILE)
		return true;
	else
		return false;
}


void AsteroidObject::OnUpdate(float deltaTime)
{
	if(position.x > 100)
	{
		velocity.x = -5;
	}
	if(position.x < -100)
	{
		velocity.x = 5;
	}
	if(position.y > 100)
	{
		velocity.y = -5;
	}
	if(position.y > -100)
	{
		velocity.y = 5;
	}
}

void AsteroidObject::OnCollision(GameObject * otherObject)
{
	//action for the asteroid, when it hits the ship
	//asteroid flagged to be removed here, ship will be damage in the ship code. 
	if(otherObject->GetType() == GAMEOBJECT_ASTEROID || otherObject->GetType() == GAMEOBJECT_PLAYER)
	{
		//Vector2 diff = position - otherObject->GetPosition();
		//float length = sqrtf((diff.x * diff.x) + (diff.y * diff.y));
		//Vector2 normDirection = diff / length;
		velocity = velocity * -1.0f; //Vector2(velocity.x * normDirection.x, velocity.y * normDirection.y); //reverse on collision
	}
	else if(otherObject->GetType() == GAMEOBJECT_PROJECTILE)
	{
		isDead = true;
	}


}

void AsteroidObject::OnDestroy()
{
	//when removed, add two smaller asteroids. 
	if(width > 1.1f)//if it was large
	{
		AsteroidObject * asteroid = new AsteroidObject();
		asteroid->SetPosition(Vector2(position.x+1.1f, position.y +1.1f));
		asteroid->HalfSize();
		asteroid->SetTexture(ImageLibrary::ASTEROID_IMG);
		CurrentGameWorld::CURRENTWORLD->Add(asteroid);

		AsteroidObject * asteroid2 = new AsteroidObject();
		asteroid2->SetTexture(ImageLibrary::ASTEROID_IMG);
		asteroid->HalfSize();
		asteroid2->SetPosition(Vector2(position.x-1.1f, position.y -1.1f));
		CurrentGameWorld::CURRENTWORLD->Add(asteroid2);
	}
	
}
