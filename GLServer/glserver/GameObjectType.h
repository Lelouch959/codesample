#pragma once

/**
Used to seperate the collision types into groups. Player may collide with a DOODAD but maybe not PROJECTILE. 
*/
class GameObjectType
{
public:
	GameObjectType(int);
	~GameObjectType(void);

	void SetType(int);
	int GetType();

	#define EMPTY 0  //empty type, no collision
	#define PLAYER 1 
	#define DOODAD 2 
	#define PROJECTILE 3
	
private:
	int type;
};

