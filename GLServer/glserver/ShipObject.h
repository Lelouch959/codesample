#pragma once
#include "gameobject.h"
class ShipObject :
	public GameObject
{
public:

	ShipObject(void);
	~ShipObject(void);
	
	void TurnLeft();
	void TurnRight();
	void Accelerate();
	void Decelerate();
	void Fire();
	
	int GetHealth();

	void OnUpdate(float);
	bool CollidesWithType(int);
	void OnCollision(GameObject *);

private:
	int health;
	float fireDelay;
	float damageDelay;
	mutable GameObject * lastFired;
};

