#include "ProjectileObject.h"

#include "ObjectTypes.h"

ProjectileObject::ProjectileObject(void)
{
	width = 0.3f;
	height = 0.4f;
	position = velocity = Vector2(0,0);
	type = GAMEOBJECT_PROJECTILE;
	//start facing up
	direction = Vector2(0,1.0f);
	rotation = 0;
	timeSinceSpawn = 0;
	Create();
}


ProjectileObject::~ProjectileObject(void)
{
	

}

void ProjectileObject::OnUpdate(float deltaTime)
{
	if(timeSinceSpawn < 1.0f) //time untill it has been alive 0.5 seconds , so not collide with own ship. 
	{
		timeSinceSpawn += deltaTime;
	}

}
bool ProjectileObject::CollidesWithType(int tType)
{
	if(timeSinceSpawn >= 1.0f)
	{
		if(tType == GAMEOBJECT_ASTEROID || tType == GAMEOBJECT_PLAYER)
			return true;
		else
			return false;
	}
	return false;
}

void ProjectileObject::OnCollision(GameObject *)
{
	//action for the asteroid, when it hits the ship
	//asteroid flagged to be removed here, ship will be damage in the ship code. 
	isDead = true;
}
