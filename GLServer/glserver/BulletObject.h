#pragma once
#include "gameobject.h"
#include "ObjectTypes.h"
class BulletObject :
	public GameObject
{
public:
	
	BulletObject(void);
	~BulletObject(void);

	bool CollidesWithType(int);
	void ActionForType(int);

	void OnDestroy();
};

