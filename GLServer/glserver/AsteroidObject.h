#pragma once
#include "gameobject.h"
#include "ObjectTypes.h"
class AsteroidObject : public GameObject
{
public:



	AsteroidObject();
	~AsteroidObject(void);
	
	void HalfSize();
	void OnUpdate(float);
	bool CollidesWithType(int);
	void OnCollision(GameObject *);

	void OnDestroy();
};

