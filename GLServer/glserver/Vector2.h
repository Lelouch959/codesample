#pragma once
class Vector2
{
public:
	Vector2(void);
	Vector2(float, float);
	~Vector2(void);

	Vector2& operator+(Vector2);
	Vector2& operator-(Vector2);
	Vector2& operator*(float);
	Vector2& operator/(float);
	void Rotate(float);

	float x;
	float y;
};

