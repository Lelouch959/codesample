#include "User.h"

User::User()
{
	//-1 no buffer ready
	completeBufferIndex = -1;
	buffer = new char*[MAXBUFFERS]();
	for(int i = 0 ; i < MAXBUFFERS ; i ++)
	{
		buffer[i] = NULL;
	}

	//start all controlls up
	controls = new BYTE[NUMCONTROLS]();
	for(int i = 0 ; i < NUMCONTROLS ; i++)
	{
		controls[i] = 0;
	}
}

User::~User()
{
}

void User::SetControls(char* sentControls)
{
	for(int i = 0 ; i < NUMCONTROLS ; i++)
	{
		controls[i] = (BYTE)sentControls[i];
	}
}

void User::ControlUpdate()
{
	if(controls[LEFT])
		playerUnit->TurnLeft();
	if(controls[RIGHT])
		playerUnit->TurnRight();
	if(controls[UP])
		playerUnit->Accelerate();
	if(controls[DOWN])
		playerUnit->Decelerate();
	if(controls[FIRE])
		playerUnit->Fire();
}

void User::SetData(char* recentBuffer)
{
	//work everything in temp, so that only when its ready it can be accessed;
	int tempBufferIndex = completeBufferIndex;
	
	tempBufferIndex++;
	
	if(tempBufferIndex >= MAXBUFFERS)
	{
		tempBufferIndex = 0;
	}
	
	if(buffer[tempBufferIndex] != NULL )
	{
		delete  buffer[tempBufferIndex];
		buffer[tempBufferIndex] = NULL;
	}

	buffer[tempBufferIndex] = recentBuffer;

	completeBufferIndex = tempBufferIndex;
	
	//Now it is ready to be read, any new reads will be from here. 
}

void User::Draw()
{
	for(int i = 0 ; i < playerUnit->GetHealth(); i++)
	{
		glColor3f(1.0f,0.0f,0.0f);
		glRectf(0 + (i*2) +(1*i) ,0,2 + (i*2)+(1*i),5);
		glFinish();
	}
}

float User::GetX()
{
	return playerUnit->GetPosition().x;
}

float User::GetY()
{
	return playerUnit->GetPosition().y;
}

void User::AddPlayerUnit(ShipObject* unit)
{
	playerUnit = unit;
}
