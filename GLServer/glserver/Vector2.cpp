#include "Vector2.h"
#include <math.h>
float DEGTORAD;
Vector2::Vector2(void)
{
	DEGTORAD = 3.141592653589793f / 180.0f;
}

Vector2::Vector2(float tX, float tY)
{
	x = tX;
	y = tY;
}


Vector2::~Vector2(void)
{
}

Vector2& Vector2::operator+(Vector2 other)
{
	Vector2 result;
	result.x = other.x + x;
	result.y = other.y + y;
	return result;
}

Vector2& Vector2::operator-(Vector2 other)
{
	Vector2 result;
	result.x =  x - other.x;
	result.y =  y - other.y;
	return result;
}


Vector2& Vector2::operator*(float multi)
{
	Vector2 result;
	result.x = multi * x;
	result.y = multi * y;
	return result;
}

Vector2& Vector2::operator/(float divide)
{
	Vector2 result;
	if(result.x != 0)
		result.x = x / divide;
	if(result.y != 0)
		result.y = y / divide;
	return result;
}

void Vector2::Rotate(float rotation)
{
	float s = sinf(rotation * DEGTORAD);
	float c = cosf(rotation * DEGTORAD);

	// rotate point
	float tempX = (x * c) + (y * -s);
	float tempY = (x * s) + (y * c);

	x = tempX;
	y = tempY;
}
