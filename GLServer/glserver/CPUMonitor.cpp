#include "CPUMonitor.h"

//http://stackoverflow.com/questions/13218567/how-to-get-system-cpu-ram-usage-in-c-on-windows
CPUMonitor::CPUMonitor(void)
{
}



void CPUMonitor::init(){
    SYSTEM_INFO sysInfo;
    FILETIME ftime, fsys, fuser;


    GetSystemInfo(&sysInfo);
    numProcessors = sysInfo.dwNumberOfProcessors;


    GetSystemTimeAsFileTime(&ftime);
    memcpy(&lastCPU, &ftime, sizeof(FILETIME));


    self = GetCurrentProcess();
    GetProcessTimes(self, &ftime, &ftime, &fsys, &fuser);
    memcpy(&lastSysCPU, &fsys, sizeof(FILETIME));
    memcpy(&lastUserCPU, &fuser, sizeof(FILETIME));
}


double CPUMonitor::getCurrentValue(){
    FILETIME ftime, fsys, fuser;
    ULARGE_INTEGER now, sys, user;
    double percent;


    GetSystemTimeAsFileTime(&ftime);
    memcpy(&now, &ftime, sizeof(FILETIME));


    GetProcessTimes(self, &ftime, &ftime, &fsys, &fuser);
    memcpy(&sys, &fsys, sizeof(FILETIME));
    memcpy(&user, &fuser, sizeof(FILETIME));
    percent = (sys.QuadPart - lastSysCPU.QuadPart) +
        (user.QuadPart - lastUserCPU.QuadPart);
    percent /= (now.QuadPart - lastCPU.QuadPart);
    percent /= numProcessors;
    lastCPU = now;
    lastUserCPU = user;
    lastSysCPU = sys;


    return percent * 100;
}

   // lastCPU = now;
    //lastSysCPU = timeSample.tms_stime;
    //lastUserCPU = timeSample.tms_utime;


  //  return percent;
//}
