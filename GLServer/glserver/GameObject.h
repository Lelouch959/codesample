#pragma once

#include <glew.h>
#include <glut.h>
#include "Vector2.h"
#include <vector>

class GameObject
{
public:

	//defin values
	#define GAMEOBJECT_NOTYPE -1

	//const/dest
	GameObject(void);
	~GameObject(void);

	//main calls
	void	Update(float);
	void	Draw();

	//usable methods
	void	Rotate(float);
	void	CollisionCheck(GameObject*);

	bool	ShouldDie();
	//property modifiers
	void	SetPosition(Vector2);
	Vector2	GetPosition();

	void	SetVelocity(Vector2);

	void	SetTexture(GLuint);

	float	GetWidth();
	float	GetHeight();

	void	SetType(int);
	int		GetType();

	
	//method called when removing this object
	virtual void OnDestroy() {}; //default nothing happens, object simply removed.
	
protected:
	//internal methods
	void Create(); //create this object - construct points
	//overwrite these to create collision for different object type
	virtual bool CollidesWithType(int) { return false; }; //default no collision
	virtual void OnCollision(GameObject *) {}; //default no action
	
	//custom updateLoop
	virtual void OnUpdate(float) {};


	//properties
	Vector2 position; //also centre point for rotation
	Vector2 velocity; //current velocity of object
	Vector2 direction; //direction the object is facing

	Vector2 points[4];

	bool isDead; //objects marked as dead are removed after the current update. calling OnDestroy()

	float	timePassed;
	float	width, height;
	float	rotation;
	int		type; // type is used to group collision and response types, -1 is used for no collision, 
	GLuint	mTexture;

};

