#pragma once

#include <vector>
#include "GameObject.h"

class GameWorld
{
public:
	GameWorld(void);
	~GameWorld(void);


	void Add(GameObject*);
	void Update(float);
	void Draw(Vector2 centrePoint);
	void AddAllToSpawn();
	void RemoveDead();
	
	virtual void OnCreate() {};
protected:
	
	virtual void OnUpdate() {}; // any world specific updates can be done here. 

private:
	std::vector<GameObject*> mObjectsToAdd;
	std::vector<GameObject*> mObjects;
};

