#include "GameObject.h"
#include "GameWorld.h"

GameObject::GameObject(void)
{
	//give defaults
	position = velocity = Vector2(0,0);
	width  = height = 1.0f;
	rotation = 0;
	type = GAMEOBJECT_NOTYPE;
}


GameObject::~GameObject(void)
{
}

void GameObject::Create()
{
	isDead = false;
	points[0] = Vector2(width*0.5f, - (height*0.5f));
	points[1] = Vector2(-(width*0.5f),- (height*0.5f));
	points[2] = Vector2(-(width*0.5f), height*0.5f);
	points[3] = Vector2(width*0.5f, height*0.5f);
	
}

void GameObject::Update(float deltaTime)
{
	position = position + (velocity * deltaTime);
	OnUpdate(deltaTime);
}

void GameObject::CollisionCheck(GameObject * otherObject)
{
	if(CollidesWithType(otherObject->GetType()))
	{
		float x2, y2, r2;
		x2 = otherObject->GetPosition().x;
		y2 = otherObject->GetPosition().y;
		r2 = otherObject->GetWidth() /2; //could check width height for largest, if needed. assume all squares for this. 

		float x1, y1, r1;
		x1 = GetPosition().x;
		y1 = GetPosition().y;
		r1 = GetWidth() *0.5; //could check width height for largest, if needed. assume all squares for this. 

		float dx = x2 - x1;
		float dy = y2 - y1;
		float radii = r1 + r2;
		//optimised collision http://cgp.wikidot.com/circle-to-circle-collision-detection
		if ( ( dx * dx )  + ( dy * dy ) < radii * radii ) 
		{
			OnCollision(otherObject);
		}
	}
}
void GameObject::Draw()
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glBegin(GL_QUADS);
	glTexCoord2f(0,1);
	glVertex2f(position.x + points[0].x, position.y + points[0].y);

	glTexCoord2f(1,1);
	glVertex2f(position.x + points[1].x, position.y + points[1].y);

	glTexCoord2f(1,0);
	glVertex2f(position.x + points[2].x, position.y + points[2].y);

	glTexCoord2f(0,0);
	glVertex2f(position.x + points[3].x, position.y + points[3].y);

	glEnd();
	glDisable(GL_TEXTURE_2D);

}

bool GameObject::ShouldDie()
{
	return isDead;
}

void GameObject::SetType(int tType)
{
	type = tType;
}

int	GameObject::GetType()
{
	return type;
}

void GameObject::SetVelocity(Vector2 tVelocity)
{
	velocity = tVelocity;
}
void GameObject::SetPosition(Vector2 tPosition)
{
	position = tPosition;
}

void GameObject::SetTexture(GLuint texture)
{
	mTexture = texture;
}

Vector2 GameObject::GetPosition()
{
	return position;
}

float GameObject::GetWidth()
{
	return width;
}

float  GameObject::GetHeight()
{
	return height;
}
void GameObject::Rotate(float r)
{
	direction.Rotate(r);
	for(int i = 0 ; i < 4 ; i++)
	{
		points[i].Rotate(r);
	}
	rotation+= r;
}

