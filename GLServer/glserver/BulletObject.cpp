#include "BulletObject.h"


BulletObject::BulletObject(void)
{
}


BulletObject::~BulletObject(void)
{
}

bool BulletObject::CollidesWithType(int tType)
{
	if(tType == GAMEOBJECT_ASTEROID || tType == GAMEOBJECT_PLAYER)
		return true;
	else
		return false;
}

void BulletObject::ActionForType(int tType)
{
	//when projectile hits something, destroy the projectile
	if(tType == GAMEOBJECT_ASTEROID || tType == GAMEOBJECT_PLAYER)
	{
		isDead = true;
	}
}

void BulletObject::OnDestroy()
{
	
}

