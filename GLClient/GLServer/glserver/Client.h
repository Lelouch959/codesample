#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>

#pragma comment(lib, "Ws2_32.lib")

class Client
{
public:

	#define DEFAULT_COUNT		20
	#define DEFAULT_PORT		5150
	#define DEFAULT_BUFFER      2048
	#define DEFAULT_MESSAGE     "TEST CLIENT MESS"

	Client(void);
	~Client(void);


	bool initilise();
	void Clean();
	void Update(BYTE*);
	void end();
	char* getPixelData();

	SOCKET sClient;

	char   * szBuffer;
	char  szServer[128],        // Server to connect to
	szMessage[1024];        // Message to send to sever
	int   iPort;  // Port on server to connect to
	DWORD dwCount; // Number of times to send message
	BOOL  bSendOnly;         // Send data only; don't receive
	char * pixelData;
};

