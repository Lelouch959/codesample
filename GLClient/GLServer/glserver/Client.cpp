
#include "Client.h"
#include <glut.h>
char* bufferData;

Client::Client(void)
{

	iPort     = DEFAULT_PORT;
	dwCount   = DEFAULT_COUNT;
	bSendOnly = FALSE;  
	pixelData =  new char[500*500*3];
	szBuffer = NULL;
}


Client::~Client(void)
{
}

bool Client::initilise()
{
	WSADATA       wsd;
	
	int           ret,i;
	struct sockaddr_in server;
	struct hostent    *host = NULL;
	
	if (WSAStartup(MAKEWORD(2,2), &wsd) != 0)
	{
		return false;
	}
	
	strcpy_s(szMessage, DEFAULT_MESSAGE);

	sClient = socket(AF_INET, SOCK_STREAM  , 0);

	if (sClient == INVALID_SOCKET)
	{
		OutputDebugStringW(L"Invalid socket");
		return false;
	}

	strcpy_s(szServer, "127.0.0.1");
	server.sin_family = AF_INET;
	server.sin_port = htons(iPort);
	server.sin_addr.s_addr = inet_addr(szServer);

	if (server.sin_addr.s_addr == INADDR_NONE)
    {
        host = gethostbyname(szServer);
        if (host == NULL)
        {
            return 1;
        }
        CopyMemory(&server.sin_addr, host->h_addr_list[0],
            host->h_length);
    }
    if (connect(sClient, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
    {
		OutputDebugStringW(L"Connection failed");
        return false;
    }

   
    return 0;
}

void Client::Clean()
{
	delete szBuffer;
	szBuffer = NULL;
}

void Client::Update(BYTE* controls)
{
	// Send and receive data
    //
	
	int ret;
	
	OutputDebugStringW(L"SendING");
	bool running = true;
    while(running)
    {
        ret = send(sClient, (char*)controls, 5, 0);
        if (ret == 0)
            break;
        else if (ret == SOCKET_ERROR)
        {
            break;
        }
		if(szBuffer != NULL)
		{
			delete szBuffer;
		}
		szBuffer = new char[500*500*3];
		ret = recv(sClient, szBuffer, 500*500*3, MSG_WAITALL);
        if (ret == 0)        // Graceful close
            break;
        else if (ret == SOCKET_ERROR)
        {
            break;
        }

		
		pixelData = szBuffer;

		glutPostRedisplay();

		running = false;
    }
}

char* Client::getPixelData()
{
	return pixelData;
}

void Client::end()
{
	closesocket(sClient);
	delete szBuffer;
    WSACleanup();
}
