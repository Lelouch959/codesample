// MMOServer.cpp : Defines the entry point for the console application.
//


#include <stdio.h>
#include <stdlib.h>
#include <glew.h>
#include <glut.h>
#include <time.h>
#include <assert.h>
#include "Client.h"
#include <iostream>
#include <thread>
#include <conio.h>

#pragma comment(lib, "glew32.lib")

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

#define LEFT 0
#define UP 1
#define RIGHT 2
#define DOWN 3
#define FIRE 4

static const int texSize = 500;
static const int N = texSize*texSize;


#define GLEW_STATIC
#define NUMCONTROLS 5
void init(int argc, char **argv);
void display();
void GetPixelBuffer();
void recieveNormalControls(unsigned char key, int x, int y);
void recieveNormalControlsUp(unsigned char key, int x, int y);
void recieveControls(int key, int x, int y);
void recieveControlsUp(int key, int x, int y);

Client * userClient;
char * pixelData;
BYTE * controlStack;

float renderTiming = 0;
int renderCount = 0;
int lastRenderTime = 0;
int currentRenderTime = 0;

float lastCommandSent = 0;
float frameRecieved = 0;
float latency = 0;

int main(int argc, char **argv) 
{
	controlStack = new BYTE[NUMCONTROLS]();
	for(int i = 0 ; i < NUMCONTROLS ; i++)
	{
		controlStack[i] = 0;
	}
	userClient = new Client();
	userClient->initilise();
    init(argc, argv);
	glutMainLoop();

    return 0;
}

void recieveNormalControls(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 32:
			//space pressed
			controlStack[FIRE] = 1;
		break;
	}
}
void recieveNormalControlsUp(unsigned char key, int x, int y)
{
		switch (key)
	{
		case 32:
			//space pressed
			controlStack[FIRE] = 0;
		break;
	}
}
void recieveControls(int key, int x, int y)
{
	switch (key)
	{
		case 100://LEFT
			controlStack[LEFT] = 1;
		break;
		case 101://UP
			controlStack[UP] = 1;
		break;
		case 102://RIGHT
			controlStack[RIGHT] = 1;
		break;
		case 103://DOWN
			controlStack[DOWN] = 1;
		break;
	
	}	
}

void recieveControlsUp(int key, int x, int y)
{
	switch (key)
	{
		case 100://LEFT
			controlStack[LEFT] = 0;
		break;
		case 101://UP
			controlStack[UP] = 0;
		break;
		case 102://RIGHT
			controlStack[RIGHT] = 0;
		break;
		case 103://DOWN
			controlStack[DOWN] = 0;
		break;
	
	}	
}


void init(int argc, char **argv)
{
	//
    // set up glut to get valid GL context and 
    // get extension entry points
    //
    glutInit (&argc, argv);

	int width = 500;
	int height = 500;

	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	
	glutInitWindowPosition (50, 50);
	glutInitWindowSize (width, height); 
    glutCreateWindow("STREAMING TUTORIAL");

	glutDisplayFunc(display);
	glutKeyboardFunc(recieveNormalControls);
	glutKeyboardUpFunc(recieveNormalControlsUp);
	glutSpecialFunc(recieveControls);
	glutSpecialUpFunc(recieveControlsUp);
	glEnable(GL_DEPTH_TEST);
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glewInit();
	
}

void display()
{
	frameRecieved = GetTickCount() * 0.001;
	if(latency > 0 )
	{
		latency = latency + ( frameRecieved - lastCommandSent) *0.5;
	}
	else 
		latency =  frameRecieved - lastCommandSent;

	currentRenderTime = GetTickCount();
	float deltaTime = (	currentRenderTime - lastRenderTime) * 0.001f;

	renderTiming+= deltaTime; // delta time is seconds, 
	if(renderTiming > 1.0f)//a second past
	{
		renderTiming = 0; //reset clock for a second
		printf("Render :: RenderLoopsPerSecond  %d \n", renderCount);
		printf("Latency Average ::  %f \n", latency);
		latency = 0;
		renderCount = 0;
	}
	lastRenderTime = currentRenderTime;
	pixelData = userClient->getPixelData();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glLoadIdentity();
	GLbyte data[500*500*3];
	for(int i = 0 ; i < 500*500*3 ; i++ )
	{
		data[i] =(GLbyte)pixelData[i];
	
	}
	glDrawPixels(500,500,GL_RGB,GL_UNSIGNED_BYTE,data);
	glFinish();
	glutSwapBuffers(); //Send the 3D scene to the screen
	userClient->Clean();
	lastCommandSent = GetTickCount() * 0.001;
	userClient->Update(controlStack);
	renderCount++;
	
}

